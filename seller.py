#Seller Class
#1/12/16
#Brady Russ

class Seller:
    def __init__(self,username,userid,reliability,location,email):
        self.username = username
        self.userid = userid
        self.reliability = reliability
        self.location = location
        self.email = email

    def getName(self):
        return self.username

    def getId(self):
        return self.userid

    def getRel(self):
        return self.reliability

    def getLoc(self):
        return self.location

    def getEmail(self):
        return self.email

