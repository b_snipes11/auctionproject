# Auction Tester
# 1/12/16
# Brady Russ

from seller import *

username = input("enter a username  ")
userid = int(input("enter a 5 digit identification number  "))
reliability = int(input("enter a percent reliability out of 100  "))
location = input("enter location  ")
email = input("enter email  ")
seller1 = Seller(username,userid,reliability, location, email)
print(seller1.getName())
print(seller1.getId())
print(seller1.getRel())
print(seller1.getLoc())
print(seller1.getEmail())

from item import *

title = input("enter item title  ")
description = input("enter item description  ")
condition = input("enter item condition  ")
startingprice = input("enter starting price  ")
reserveprice = input("enter reserve price  ")
buynowprice = input("enter 'Buy Now' price  ")
shippingprice = input("enter shipping price  ")
timeremaining = input("enter remaining time  ")

from buyer import *
def newBuyer():
    username = input("enter a buyer username  ")
    userid = int(input("enter a 5 digit user identification number  "))
    payment = input("enter either bid or 'Buy Now' price  ")
    address = input("enter shipping address  ")
    email = input("enter user email  ")
    return Buyer(username,userid,payment,address,email)

def printBuyer(buyer):
    print(buyer.getName())
    print(buyer.getId())
    print(buyer.getPay())
    print(buyer.getAdd())
    print(buyer.getEmail())

buyer1 = newBuyer()
buyer2 = newBuyer()
buyer3 = newBuyer()
printBuyer(buyer1)




